import * as types from './contacts.actionTypes';

export const getContactsActionCreator = contacts => {
  return { type: types.GET_CONTACTS, contacts };
};

export const addContactActionCreator = contact => {
  return { type: types.ADD_CONTACT, contact };
};

export const updateContactActionCreator = contact => {
  return { type: types.UDPATE_CONTACT, contact };
};

export const deleteContactActionCreator = contactId => {
  return { type: types.DELETE_CONTACT, contactId };
};

export const setContactSearchValueActionCreator = value => {
  return { type: types.SET_CONTACT_SEARCH_VALUE, value };
};
