import NewContact from './NewContact';
import { withFormik } from 'formik';
import { newContactSchema } from '../../validation/contactSchemas';

export default withFormik({
  enableReinitialize: true,

  mapPropsToValues: props => {
    const { currentContact } = props;

    let values = {
      id: '',
      name: '',
      phone: '',
      email: ''
    };

    if (currentContact) {
      values = {
        ...values,
        id: currentContact.id,
        name: currentContact.name,
        phone: currentContact.phone,
        email: currentContact.email
      };
    }

    return values;
  },

  validationSchema: newContactSchema,

  handleSubmit: (values, { props }) => {
    const { addContact, updateContact, history } = props;
    const { id, name, phone, email } = values;

    if (id !== '') {
      updateContact({ id, name, phone, email });
      history.goBack();
    } else {
      addContact({ name, phone, email });
      history.goBack();
    }
  }
})(NewContact);
