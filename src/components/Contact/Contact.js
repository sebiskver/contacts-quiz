import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import { withRouter } from 'react-router-dom';
import * as routes from '../../constants/routes';

class Contact extends Component {
  handleRedirectToUpdateContact = contactId => {
    const { history } = this.props;
    history.push(routes.UPDATE_CONTACT.replace(':contactId', contactId));
  };

  render() {
    const { contact } = this.props;

    return (
      <Card
        className="w-100 mb-2"
        onClick={() => this.handleRedirectToUpdateContact(contact.id)}
      >
        <Card.Body>
          <p>Name: {contact.name}</p>
          <p>Phone: {contact.phone}</p>
          <p>Email: {contact.email}</p>
        </Card.Body>
      </Card>
    );
  }
}

export default withRouter(Contact);
