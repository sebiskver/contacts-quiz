import * as types from './contacts.actionTypes';

const initState = {
  contacts: [],
  searchContactValue: ''
};

export default (state = initState, action) => {
  switch (action.type) {
    case types.GET_CONTACTS:
      return {
        ...state,
        contacts: [...action.contacts]
      };

    case types.ADD_CONTACT:
      return {
        ...state,
        contacts: [action.contact, ...state.contacts]
      };

    case types.UDPATE_CONTACT:
      return {
        ...state,
        contacts: state.contacts.map(contact =>
          contact.id === action.contact.id ? { ...action.contact } : contact
        )
      };

    case types.DELETE_CONTACT:
      return {
        ...state,
        contacts: state.contacts.filter(
          contact => contact.id !== action.contactId
        )
      };

    case types.SET_CONTACT_SEARCH_VALUE:
      return {
        ...state,
        searchContactValue: action.value
      };

    default:
      return state;
  }
};
