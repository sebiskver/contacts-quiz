import {
  getDataFromLocalStorage,
  setDataToLocalStorage
} from '../../utils/localStorage';
import {
  getContactsActionCreator,
  addContactActionCreator,
  updateContactActionCreator,
  deleteContactActionCreator,
  setContactSearchValueActionCreator
} from './contacts.actionCreators';
import uuid from 'uuid/v1';
import { LOCAL_STORAGE_CONTACTS } from '../../constants/misc';

export const getContacts = () => {
  const contacts = getDataFromLocalStorage(LOCAL_STORAGE_CONTACTS);

  if (contacts === null) {
    return getContactsActionCreator([]);
  }

  return getContactsActionCreator(contacts);
};

export const addContact = formValues => {
  const contact = { id: uuid(), ...formValues };
  let contacts = getDataFromLocalStorage(LOCAL_STORAGE_CONTACTS);

  if (contacts === null) contacts = [];

  setDataToLocalStorage(LOCAL_STORAGE_CONTACTS, [contact, ...contacts]);

  return addContactActionCreator(contact);
};

export const updateContact = formValues => {
  const contactId = formValues.id;
  let contacts = getDataFromLocalStorage(LOCAL_STORAGE_CONTACTS);

  if (contacts === null) contacts = [];

  contacts = contacts.map(contact =>
    contact.id === contactId ? { ...formValues } : contact
  );

  setDataToLocalStorage(LOCAL_STORAGE_CONTACTS, [...contacts]);

  return updateContactActionCreator(formValues);
};

export const deleteContact = contactId => {
  let contacts = getDataFromLocalStorage(LOCAL_STORAGE_CONTACTS);

  if (contacts === null) contacts = [];

  contacts = contacts.filter(contact => contact.id !== contactId);

  setDataToLocalStorage(LOCAL_STORAGE_CONTACTS, [...contacts]);

  return deleteContactActionCreator(contactId);
};

export const setContactSearchValue = value => {
  return setContactSearchValueActionCreator(value);
};
