import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';

class Search extends Component {
  handleSearchChange = event => {
    const { onSearch } = this.props;
    onSearch(event.target.value);
  };

  render() {
    const { value } = this.props;

    return (
      <Form className="py-2 border-bottom border-top">
        <Form.Control
          type="text"
          placeholder="Search"
          name="searchValue"
          value={value}
          onChange={this.handleSearchChange}
        />
      </Form>
    );
  }
}

export default Search;
