import Home from './Home';
import { connect } from 'react-redux';
import {
  selectContacts,
  selectSearchContactValue,
  selectFilteredContacts
} from '../../store/contacts/contacts.selectors';
import {
  getContacts,
  setContactSearchValue
} from '../../store/contacts/contacts.actions';

const mapStateToProps = state => {
  return {
    searchValue: selectSearchContactValue(state),
    contacts: selectFilteredContacts(
      selectContacts(state),
      selectSearchContactValue(state)
    )
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContacts()),
    setContactSearchValue: value => dispatch(setContactSearchValue(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
