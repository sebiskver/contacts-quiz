import Yup from '../utils/yup';

export const newContactSchema = Yup.object().shape({
  name: Yup.string().required('Please enter the name'),
  phone: Yup.string().required('Please enter the phone'),
  email: Yup.string()
    .email()
    .required('Please enter the email')
});
