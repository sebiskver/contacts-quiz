export const selectContacts = state => {
  return state.contacts.contacts;
};

export const selectCurrentContact = (contacts, contactId) => {
  return contacts.find(contact => contact.id === contactId);
};

export const selectSearchContactValue = state => {
  return state.contacts.searchContactValue;
};

export const selectFilteredContacts = (contacts, searchValue) => {
  return contacts.filter(contact =>
    contact.name.toLowerCase().includes(searchValue.toLowerCase())
  );
};
