import { createStore, combineReducers, compose } from 'redux';
import contactsReducer from './contacts/contacts.reducers';

const store = combineReducers({
  contacts: contactsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(store, composeEnhancers());
