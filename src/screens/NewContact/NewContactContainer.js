import NewContactForm from './NewContactForm';
import { connect } from 'react-redux';
import {
  addContact,
  getContacts,
  updateContact,
  deleteContact
} from '../../store/contacts/contacts.actions';
import {
  selectContacts,
  selectCurrentContact
} from '../../store/contacts/contacts.selectors';

const mapStateToProps = (state, props) => {
  const { match } = props;
  const contactId = match.params.contactId;

  return {
    contacts: selectContacts(state),
    currentContact: selectCurrentContact(selectContacts(state), contactId)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContacts()),
    addContact: contact => dispatch(addContact(contact)),
    updateContact: contact => dispatch(updateContact(contact)),
    deleteContact: contactId => dispatch(deleteContact(contactId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewContactForm);
