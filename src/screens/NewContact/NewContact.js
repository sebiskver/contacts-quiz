import React, { Component } from 'react';
import * as routes from '../../constants/routes';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import { ErrorMessage } from 'formik';

class NewContact extends Component {
  componentDidMount() {
    const { contacts, getContacts } = this.props;
    if (!contacts.length) getContacts();
  }

  handleDeleteContact = contactId => {
    const { deleteContact, history } = this.props;
    deleteContact(contactId);
    history.goBack();
  };

  render() {
    const { values, handleSubmit, handleChange } = this.props;

    return (
      <Container>
        <h2 className="mt-2 py-2 border-bottom">
          {values.id ? 'Update Contact' : 'New Contact'}
        </h2>

        <Link to={routes.HOME}>
          <Button variant="danger" className="mb-2">
            Go Back
          </Button>
        </Link>

        <Form className="py-2 border-bottom border-top" onSubmit={handleSubmit}>
          <Form.Control
            type="text"
            placeholder="Enter Name"
            className="mb-2"
            name="name"
            value={values.name}
            onChange={handleChange}
          />
          <ErrorMessage component="div" name="name" />

          <Form.Control
            type="text"
            placeholder="Enter Phone"
            className="mb-2"
            name="phone"
            value={values.phone}
            onChange={handleChange}
          />
          <ErrorMessage component="div" name="phone" />

          <Form.Control
            type="text"
            placeholder="Enter Email"
            className="mb-2"
            name="email"
            value={values.email}
            onChange={handleChange}
          />
          <ErrorMessage component="div" name="email" />

          <Button variant="primary" type="submit">
            Submit
          </Button>

          {values.id && (
            <Button
              variant="danger"
              className="ml-2"
              onClick={() => this.handleDeleteContact(values.id)}
            >
              Delete
            </Button>
          )}
        </Form>
      </Container>
    );
  }
}

export default NewContact;
