import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import * as routes from './constants/routes';
import HomeContainer from './screens/Home/HomeContainer';
import NewContactContainer from './screens/NewContact/NewContactContainer';

class App extends Component {
  render() {
    return (
      <Switch>
        <Route
          path={[routes.NEW_CONTACT, routes.UPDATE_CONTACT]}
          component={NewContactContainer}
          exact
        />
        <Route path={routes.HOME} component={HomeContainer} exact />
      </Switch>
    );
  }
}

export default App;
