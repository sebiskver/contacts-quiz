export const getDataFromLocalStorage = item => {
  return JSON.parse(localStorage.getItem(item));
};

export const setDataToLocalStorage = (item, data) => {
  return localStorage.setItem(item, JSON.stringify(data));
};
