import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import * as routes from '../../constants/routes';
import Search from '../../components/Search/Search';
import Contacts from '../../components/Contacts/Contacts';

class Home extends Component {
  componentDidMount() {
    const { contacts, getContacts } = this.props;
    if (!contacts.length) getContacts();
  }

  render() {
    const { contacts, setContactSearchValue, searchValue } = this.props;

    return (
      <Container>
        <h2 className="mt-2 py-2 border-bottom">Contacts App</h2>

        <Link to={routes.NEW_CONTACT}>
          <Button variant="success" className="mb-2">
            Add New Contact
          </Button>
        </Link>

        <Search onSearch={setContactSearchValue} value={searchValue} />

        <Contacts contacts={contacts} />
      </Container>
    );
  }
}

export default Home;
