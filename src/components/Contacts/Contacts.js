import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Contact from '../Contact/Contact';

class Contacts extends Component {
  render() {
    const { contacts } = this.props;

    return (
      <Row noGutters className="mt-3">
        {!!contacts.length &&
          contacts.map(contact => (
            <Contact key={contact.id} contact={contact} />
          ))}
      </Row>
    );
  }
}

export default Contacts;
